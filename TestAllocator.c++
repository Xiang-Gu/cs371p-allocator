// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator
#include <sstream>  // istringtstream, ostringstream

#include "gtest/gtest.h"

#include "Allocator.h"

// --------------
// TestAllocator1
// --------------

template <typename A>
struct TestAllocator1 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    1000>,
    my_allocator<double, 1000>>
    my_types_1;

TYPED_TEST_CASE(TestAllocator1, my_types_1);

TYPED_TEST(TestAllocator1, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type  s = 10;
    const value_type v = 5;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator1, test_2) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 5;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// --------------
// TestAllocator2
// --------------

template <typename A>
struct TestAllocator2 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
my_allocator<int,    1000>,
             my_allocator<double, 1000>,
             my_allocator<char,   1000>>
             my_types_2;

TYPED_TEST_CASE(TestAllocator2, my_types_2);

TYPED_TEST(TestAllocator2, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator2, test_2) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TYPED_TEST(TestAllocator2, test_3) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    ASSERT_EQ(x[0], 992);
}

TYPED_TEST(TestAllocator2, test_4) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    const allocator_type x;
    ASSERT_EQ(x[0], 992);
}


// -------------------
// test initialization
// -------------------
TEST(TestAllocator, test_0) {
    my_allocator<char, 100> x;
    ASSERT_EQ(x[0], 92);
    my_allocator<int, 100> y;
    ASSERT_EQ(y[0], 92);
    my_allocator<double, 1000> z;
    ASSERT_EQ(z[0], 992);
}

// ----------------------------------------
// test allocate, construct, and deallocate
// ----------------------------------------
TEST(TestAllocator, test_1) {
    my_allocator<char, 100> x;
    char v = 'a';
    char* p = x.allocate(1);
    x.construct(p, v);
    ASSERT_EQ(*p, 'a');
}

TEST(TestAllocator, test_2) {
    my_allocator<int, 100> x;
    int v = 2;
    int size = 5;
    int* p = x.allocate(size);

    int* b = p;
    int* e = p + size;
    while (b != e) {
        x.construct(b, v);
        ++b;
    }

    ASSERT_EQ(size, std::count(p, p+size, v));
}

TEST(TestAllocator, test_3) {
    my_allocator<int, 100> x;
    int v = 2;
    int size = 5;
    int* p = x.allocate(size);

    int* b = p;
    int* e = p + size;
    while (b != e) {
        x.construct(b, v);
        ++b;
    }

    ASSERT_EQ(x[0], -20);
}


TEST(TestAllocator, test_4) {
    my_allocator<int, 100> x;
    int v = 2;
    int size = 5;
    int* p = x.allocate(size);
    x.construct(p, v);

    int* b = p;
    int* e = p + size;
    while (b != e) {
        x.destroy(p);
        ++b;
    }

    ASSERT_EQ(x[0], -20);
}

TEST(TestAllocator, test_5) {
    my_allocator<int, 100> x;
    int v = 2;
    int size = 5;
    int* p = x.allocate(size);
    x.construct(p, v);

    int* b = p;
    int* e = p + size;
    while (b != e) {
        x.destroy(p);
        ++b;
    }

    x.deallocate(p, size);

    ASSERT_EQ(x[0], 92);
}

// -----------------------------
// test iterator, const_iterator
// -----------------------------
TEST(TestAllocator, test_6) {
    my_allocator<double, 100> x;
    double v = 12.5;
    int size = 5;
    x.allocate(2);
    x.allocate(2);
    std::vector<int> sentinels;
    for (my_allocator<double, 100>::iterator it = x.begin(); it != x.end(); ++it) {
        sentinels.push_back(*it);
    }
    ASSERT_EQ(sentinels.size(), 3);
    ASSERT_EQ(sentinels[0], -16);
    ASSERT_EQ(sentinels[1], -16);
    ASSERT_EQ(sentinels[2], 44);
}

TEST(TestAllocator, test_7) {
    my_allocator<double, 100> x;
    my_allocator<double, 100>::iterator it1 = x.begin();
    my_allocator<double, 100>::iterator it2 = x.begin();
    ++it1;
    ASSERT_NE(it1, it2);
    ++it2;
    ASSERT_EQ(it1, it2);
}

TEST(TestAllocator, test_8) {
    my_allocator<double, 100> x;
    my_allocator<double, 100>::iterator it1 = x.begin();
    my_allocator<double, 100>::iterator it2 = x.begin();
    ASSERT_EQ(it1, it2);
    ++it1;
    --it1;
    ASSERT_EQ(it1, it2);
}

TEST(TestAllocator, test_9) {
    my_allocator<int, 100> x;
    x.allocate(2);
    x.allocate(3);
    x.allocate(4);
    x.allocate(5);
    const my_allocator<int, 100>& y = x;

    std::vector<int> sentinels;
    for(my_allocator<int, 100>::const_iterator cit = y.begin(); cit != y.end(); ++cit) {
        sentinels.push_back(*cit);
    }

    ASSERT_EQ(sentinels.size(), 5);
    ASSERT_EQ(sentinels[0], -8);
    ASSERT_EQ(sentinels[1], -12);
    ASSERT_EQ(sentinels[2], -16);
    ASSERT_EQ(sentinels[3], -20);
    ASSERT_EQ(sentinels[4], 4);
}

// --------------------------
// test edge case in allocate
// --------------------------
TEST(TestAllocator, test_10) {
    my_allocator<double, 100> x;
    x.allocate(11);
    ASSERT_EQ(x[0], -92);
}

TEST(TestAllocator, test_11) {
    my_allocator<double, 100> x;
    double* p1 = x.allocate(7);
    double* p2 = x.allocate(1);
    double* p3 = x.allocate(1);
    ASSERT_EQ(x[0], -56);
    ASSERT_EQ(x[96], -12);

    x.deallocate(p1, 7);
    ASSERT_EQ(x[0], 56);

    x.deallocate(p3, 1);
    ASSERT_EQ(x[96], 12);

    x.deallocate(p2, 1);
    ASSERT_EQ(x[0], 92);
}

// --------------------
// test allocator_solve
// --------------------
TEST(TestAllocator, test_12) {
    std::istringstream r("4\n\n5\n\n5\n3\n\n5\n3\n-1\n\n5\n3\n3\n-1\n-1");
    std::ostringstream w;
    allocator_solve(r, w);
    ASSERT_EQ("-40 944\n-40 -24 912\n40 -24 912\n72 -24 880", w.str());
}