// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <iostream>  // istream, ostream
#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * <your documentation>
     */
    bool valid () const {
        // <your code>
        // <use iterators>
        for (const_iterator it = begin(); it != end(); ++it) {
            // Get the value of start sentinel
            const int startSentinel = *it;
            // Get the value of end sentinel
            const char* charPtr = reinterpret_cast<const char*>(it._p);
            const int endSentinel = *reinterpret_cast<const int*>(charPtr + sizeof(int) + abs(startSentinel));

            // Compare them to check validity of current block.
            if (startSentinel != endSentinel) {
                return false;
            }
        }
        return true;
    }

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            // <your code>
            // Two iterators are considered "equal" if they both point to the same address.
            return lhs._p == rhs._p;
        }                                           // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

        // -----------
        // Allow methods in enclosing class (my_allocator) access _p.
        // -----------
        friend class my_allocator;

    private:
        // ----
        // data
        // _p points to the address of the start sentinel of each block.
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }


        int* getAddressOfStartSentinel() const {
            return _p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            // Get the value of the sentinel of current block.
            // Negative means busy block; Positive means free block.
            // Magnitude means the size (in bytes) of this block.
            return *_p;
        }            // replace!

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            // Move _p to point to the start sentinel of the next block.
            int sizeOfBlock = abs(*_p);
            // Add up bytes to charPtr (a char pointer) to point to the start
            // sentinel of next block. If we directly add to _p, pointer
            // arithmetic will interpret it as units of ints rather than bytes.
            char* charPtr = reinterpret_cast<char*>(_p);
            charPtr += 2 * sizeof(int) + sizeOfBlock;
            // Set _p (a int pointer) to also point to the start
            // sentinel of next block.
            _p = reinterpret_cast<int*>(charPtr);

            return *this;
        }

        // -----------
        // operator ++ post-increment
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // It is user's responsibility to not call -- on an iterator
        // that already points to the start sentinel of first block.
        // -----------

        iterator& operator -- () {
            // Get the size (in bytes) of the previous block.
            int sizeOfPreviousBlock = abs(*(_p - 1));

            char* charPtr = reinterpret_cast<char*>(_p);
            charPtr -= (2 * sizeof(int) + sizeOfPreviousBlock);

            _p = reinterpret_cast<int*>(charPtr);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return lhs._p == rhs._p;
        }                                                       // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

        // -----------
        // Allow methods in enclosing class (my_allocator) access _p.
        // -----------
        friend class my_allocator;

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        int* getAddressOfStartSentinel() const {
            return _p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            // Return a const reference of the value of the
            // start sentinel of current block.
            return *_p;
        }            // replace!

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            // Same as iterator::operator ++().
            int sizeOfBlock = abs(*_p);

            const char* charPtr = reinterpret_cast<const char*>(_p);
            charPtr += 2 * sizeof(int) + sizeOfBlock;

            _p = reinterpret_cast<const int*>(charPtr);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            // Similar as iterator::operator --().
            int sizeOfPreviousBlock = abs(*(_p - 1));

            char* charPtr = reinterpret_cast<char*>(_p);
            charPtr -= 2 * sizeof(int) + sizeOfPreviousBlock;

            _p = reinterpret_cast<int*>(charPtr);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        // char a[N] will automatically be evoked first, which is O(1) and contents are random.
        // Initialize my_allocator to one (large) free block with start and end sentinel being
        // N - sizeof(int).
        (*this)[0] = N - 2 * sizeof(int);
        (*this)[N - sizeof(int)] = N - 2 * sizeof(int);
        // <your code>
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n) {
        if (n >= 1) {
            // Compute requested memory in bytes.
            int requestedSize = n * sizeof(T);

            // Use iterator to search for the first free block of enough size.
            for (iterator it = begin(); it != end(); ++it) {
                // Size of current block (can be either positive or negative).
                int sizeOfBlock = *it;

                if (sizeOfBlock >= requestedSize) {
                    char* charPtr = reinterpret_cast<char*>(it._p);
                    // Split current free block to a busy block (requested) and a remaining free block.
                    if (sizeOfBlock - requestedSize >= sizeof(T) + 2 * sizeof(int)) {
                        // Compute size (in bytes) of the remaining free block after this allocation.
                        int sizeOfRemainingBlock = sizeOfBlock - requestedSize - 2 * sizeof(int);

                        // Update the start sentinel of this newly allocated block.
                        *reinterpret_cast<int*>(charPtr) = -requestedSize;
                        // Update the end sentinel of this newly allocated block.
                        charPtr += sizeof(int) + requestedSize;
                        *reinterpret_cast<int*>(charPtr) = -requestedSize;

                        // Update the start sentinel of the remaining free block.
                        charPtr += sizeof(int);
                        *reinterpret_cast<int*>(charPtr) = sizeOfRemainingBlock;
                        // Update the end sentinel of the remaining free block.
                        charPtr += sizeof(int) + sizeOfRemainingBlock;
                        *reinterpret_cast<int*>(charPtr) = sizeOfRemainingBlock;

                        // Reset charPtr to point to the start sentinel of the allocated block.
                        charPtr -= sizeOfBlock + sizeof(int);
                    }
                    else {
                        // No enough space left so allocate the whole free block to the user
                        *reinterpret_cast<int*>(charPtr) *= -1;
                        *reinterpret_cast<int*>(charPtr + sizeof(int) + sizeOfBlock) *= -1;
                    }

                    // Make sure our modification results in a valid char array.
                    assert(valid());
                    return reinterpret_cast<pointer>(charPtr + sizeof(int));
                }
            }
        }
        // Cannot find a available free block, throw an exception.
        throw std::bad_alloc();
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid.
     * Deallocate the block p points to. Concretely, p points
     * to the address of first T in the array to be deallocated).
     */
    void deallocate (pointer p, size_type n) {
        char* charPtr = reinterpret_cast<char*>(p);
        // Size of this block (can be either positive or negative).
        int startSentinel = *reinterpret_cast<int*>(charPtr - sizeof(int));
        int endSentinel = *reinterpret_cast<int*>(charPtr + abs(startSentinel));
        // Check validity by comparing start sentinel and end sentinel.
        if (startSentinel > 0 || startSentinel != endSentinel) {
            throw std::invalid_argument("invalid input pointer p.");
        }

        // Valid p, then let's deallocate it by setting the start and end sentinel.
        *reinterpret_cast<int*>(charPtr - sizeof(int)) *= -1;
        *reinterpret_cast<int*>(charPtr + abs(startSentinel)) *= -1;

        int sizeOfBlock = abs(startSentinel);

        // Coalesce with the previous block if the previous block is also free.
        if (charPtr - sizeof(int) > &a[0]) {
            int sizeOfPreviousBlock = *reinterpret_cast<int*>(charPtr - 2 * sizeof(int));
            if (sizeOfPreviousBlock > 0) {
                int sizeOfCoalescedBlock1 = sizeOfBlock + sizeOfPreviousBlock + 2 * sizeof(int);
                // Set the start sentinel of previous block.
                *reinterpret_cast<int*>(charPtr - 3 * sizeof(int) - sizeOfPreviousBlock) = sizeOfCoalescedBlock1;
                // Set the end sentinel of current block.
                *reinterpret_cast<int*>(charPtr + sizeOfBlock) = sizeOfCoalescedBlock1;

                // Update charPtr and sizeOfBlock so that these two variables have consistent meaning.
                charPtr -= 2 * sizeof(int) + sizeOfPreviousBlock;
                sizeOfBlock = sizeOfCoalescedBlock1;
            }
        }

        // Coalesce with the next block if the next block if also free.
        if (charPtr + sizeOfBlock + sizeof(int) <= &a[N-1]) {
            int sizeOfNextBlock = *reinterpret_cast<int*>(charPtr + sizeOfBlock + sizeof(int));
            if (sizeOfNextBlock > 0) {
                int sizeOfCoalescedBlock2 = sizeOfBlock + sizeOfNextBlock + 2 * sizeof(int);
                // Set the start sentinel of current block.
                *reinterpret_cast<int*>(charPtr - sizeof(int)) = sizeOfCoalescedBlock2;
                // Set the end sentinel of next block.
                *reinterpret_cast<int*>(charPtr + sizeOfCoalescedBlock2) = sizeOfCoalescedBlock2;
            }
        }

        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(reinterpret_cast<int*>(&a[0]));
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(reinterpret_cast<const int*>(&a[0]));
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(reinterpret_cast<int*>(&a[N-1] + 1));
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(reinterpret_cast<const int*>(&a[N-1] + 1));
    }
};

// ---------------
// allocator_solve
// ---------------
void allocator_solve(std::istream& in, std::ostream& out);

#endif // Allocator_h
