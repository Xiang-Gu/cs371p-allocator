// --------------
// Allocator.c++
// --------------

// --------------
// includes
// --------------
#include <cassert>      // assert
#include <iostream>     // endl, istream, ostream
#include <string>       // string, getline, stoi
#include "Allocator.h"  // my_allocator

using namespace std;

// ---------------
// allocator_solve
// ---------------
void allocator_solve(istream& in, ostream& out) {
    int numTests = 0;
    string str;

    // Get first line (number of tests)
    getline(in, str);
    numTests = stoi(str);
    // Get second line (blank line)
    getline(in, str);

    for (int i = 0; i < numTests; i++) {
        my_allocator<double, 1000> alloc;
        double* p;

        string request;
        // Get first line in each tests
        getline(in, request);

        while (request != "") {
            int requestSize = stoi(request);

            // Allocation request
            if (requestSize > 0) {
                alloc.allocate(requestSize);
            }
            // Deallocation request
            else {
                for (my_allocator<double, 1000>::iterator it = alloc.begin(); it != alloc.end(); ++it) {
                    int sizeOfBlock = *it;
                    if (sizeOfBlock < 0) {
                        if (requestSize == -1) {
                            alloc.deallocate(reinterpret_cast<double*>(it.getAddressOfStartSentinel() + 1), 0);
                            break;
                        } else {
                            requestSize += 1;
                        }
                    }
                }
            }

            // Get the next request. If it's a new blank line, in which case request = "",
            // then we will go out of this while loop. Otherwise, it's it is EOF, we will also
            // break out of the while loop.
            if(!getline(in, request)) {
                break;
            }

        }

        if (i == numTests - 1) {
            // Print out sentinel of each block separated by whitespaces
            for (my_allocator<double, 1000>::iterator it = alloc.begin(); it != --alloc.end(); ++it) {
                // increment iterator here and this trick ensures there is no whitespace after the last output.
                out << *it << " ";
            }
            out << *(--alloc.end());
        } else {
            // Print out sentinel of each block separated by whitespaces
            for (my_allocator<double, 1000>::iterator it = alloc.begin(); it != alloc.end(); ) {
                // increment iterator here and this trick ensures there is no whitespace after the last output.
                out << *it << " \n"[++it == alloc.end()];
            }
        }

    }
}