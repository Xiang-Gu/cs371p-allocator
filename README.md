# CS371p: Object-Oriented Programming Collatz Repo

* Name: Xiang Gu

* EID: xg2847

* GitLab ID: Xiang-Gu

* HackerRank ID: xiangu

* Git SHA: 378de0cbbbe9066179ab5a37235d87da147c5f5e

* GitLab Pipelines: https://gitlab.com/Xiang-Gu/cs371p-allocator/pipelines

* Estimated completion time: 20

* Actual completion time: 25

* Comments: no
